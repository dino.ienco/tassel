# TASSEL

TASSEL is a new deep-learning framework to deal with object-based SITS land cover classification which can be ascribed to the weakly supervised learning (WSL).
We leverage WSL since the object-based land cover mapping task exhibits label information at coarse granularity whose, intrinsically, brings a certain degree of approximation and inexact supervision to train the corresponding learning model. 
Our framework involves several stages: firstly it identifies the different multifaceted components on which an object is defined on. Secondly, a Convolutional Neural Network extracts an internal representation from each of the different object component. Then, the per component representation is aggregated together and used to provide the decision about the land cover class of the object. Beyond the pure model performance, our framework also allows to go a step further in the analysis providing side-information related to the contribution of each component to the final decision. Such side-information can be easily visualized in order to provide extra feedback to the end user as well as support spatial interpretability associated to the model prediction with the aim to make the black box model grey.

**The related publication will be referenced as soon as possible since it is currently under review**


# Use of TASSEL code to train and to make the inference

## MODEL TRAINING

python Tassel.py n\_timestamps n\_units output\_dir\_models trainDataFile validDataFile trainLabelFile validLabelFile

with

**n\_timestamps**: the number of time stamps of the time series data

**n\_units**: the number of features extracted by the one dimensiona CNN block

**output\_dir\_models**: the directory on which the learnt model will be saved

**trainDataFile, validDataFile**: the files containing the time series data for training and validation, respectively

**trainLabelFile, validLabelFile**: the files containing the time series land cover classes for training and validation, respectively


## MODEL INFERENCE

python Restore.py testDataFile model\_to\_load outputFileName


**testDataFile**: the file containing the time series data for the test

**model\_to\_load**: the path to the directory containing the learnt model

**outputFileName**: the name of the output file in which the restore function will save the prediction on the test set


---

# Example of use of TASSEL code to train and to make the inference

### MODEL TRAINING

Given the available data as example:

python Tassel.py 21 1024 model\_saved data/train\_0\_BoP\_2.npy data/valid\_0\_BoP\_2.npy data/train\_0\_labels.npy data/valid\_0\_labels.npy

For instance, the file data/train\_0\_BoP\_2.npy has a dimension of [5288, 2, 126] where, 5288 is the number of samples, 2 is the number of employed component and 126 is the number of time stamps (21) multiplied by the number of band for each time stamp (6)
The result is the model



### MODEL INFERENCE

Given the available data as example and the model previously produced and saved in the directory model\_saved

python Restore.py data/test_0_BoP_2.npy model_saved/model res_0.npy

This command will generate the per time series prediction in the file results/res_0.npy
This file will have as many lines as the time series in the test set and a value between 0 and 10 since the multi-class problem involves 11 land cover classes

TESTED with python packages:
- NUMPY 1.16.2
- TENSORFLOW 1.12.0 or 1.15.0


Please do not hesitate to contact me for further information or explaination about TASSEL and its related code: dino.ienco@inrae.fr